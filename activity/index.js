let http = require("http");

const server = http.createServer((request, response) => {

 
    
    if (request.url == '/register') {
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("I'm sorry the page you are looking for cannot be found."); 
    } else if (request.url == '/login') { 
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to the login page");
    } else {
        response.writeHead(404, {'Content-Type' : 'text/plain'});
        response.end("ERROR");
    }


}).listen(3000);

console.log('Server is succesfully running');