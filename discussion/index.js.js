// Node.js introduction


// Use the "require" directive load Node.js modules
// The "http module" lets Node.js trransfer data using Hyper Text Transfer Protocol(HTTP)
// HTTP is a protocol that allows the fetching of resources such as HTML documents.
// The message sent by rthe "client", usually a Web browser, called "REQUEST"
// The message sent by the "server" as an answers are called "RESPONSES"


let http = require("http");
/*

//Using this module's createServer() method, we can create an HTTP server hat listen to request on a specified port and gives responses back to client.
// A port us a virtual point where network connections start and end, each port is associated with a specific process or server 
http.createServer(function (request, response) {


    // Use the writeHead() method to:
    // Set a status code for the response - a 200 means ok
    // Set that content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type' : 'text/plain'});

    //Sends responses with trext content:
	response.end("Welcome to my Page");// response sa server

    // The server will be assigned to port 4000 via the "listen(4000)" method where server will listen to any request that are sent to it eventually communication with our server.
}).listen(4000)// dri mg comminicate ang server through localhost 

// when server is running, console will print message:
console.log('Server is running at localhost:4000');
//NOTE: BEWARE MG CTRL+C OR CLOSE PARA DI MG STOP ANG SERVER
*/


//PART 2 WITH CONDITIONALS
const server = http.createServer((request, response) => {

    // Accessing the "greeting" route returns a message of "Hello BPI"
    // "request" is object that is sent via the client(browser)
    // the ".url" property refers to the urlor link in the browser.
    
    if (request.url == '/greeting') { //in browser http://localhost:4000/greeting
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to BPI"); 
    } else if (request.url == '/customer-service') {  //in browser http://localhost:4000/customer-service
        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end("Welcome to costumer service of BPI");
    } else {
        response.writeHead(404, {'Content-Type' : 'text/plain'});
        response.end("Page is not available");
    }


}).listen(4000);

console.log('Server Running at localhost:4000');